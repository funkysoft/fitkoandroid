package eu.funkysoft.fitkomobile

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import eu.funkysoft.fitkomobile.activity.Const
import eu.funkysoft.fitkomobile.api.Model.Offer
import eu.funkysoft.fitkomobile.api.Model.Train
import eu.funkysoft.fitkomobile.api.Model.User

object DataPreferencies {
    val PREFERENCIES_NEW_USER = "preferencies_new_user"
    val PREFERENCIES_NEW_OFFER = "preferencies_new_offer"
    val PREFERENCIES_TRAIN = "preferencies_train"
    val PREFERENCIES_USER = "preferencies_user"
    val PREFERENCIES_CONF = "preferencies_conf"

    val NEW_USER = "new_user"
    val NEW_USER_COUNT = "new_user_count"
    val NEW_OFFER = "new_offer"
    val NEW_OFFER_COUNT = "new_offer_count"
    val USER = "user"
    val USER_COUNT = "user_count"
    val TRAIN = "train"
    val TRAIN_COUNT = "train_count"
    val IP_KEY = "ip_key"


    fun getPreferencies(context: Context, key: String): SharedPreferences {
        return context.getSharedPreferences(key, Context.MODE_PRIVATE)
    }

    fun getEditor(context: Context, key: String): SharedPreferences.Editor {
        return context.getSharedPreferences(key, Context.MODE_PRIVATE).edit()
    }

    fun saveIp(context:Context,ip:String){
        getEditor(context, PREFERENCIES_CONF).putString(IP_KEY,ip).apply()
    }

    fun getIp(context: Context):String{
        return getPreferencies(context, PREFERENCIES_CONF).getString(IP_KEY,Const.EMPTY)
    }

    fun putTrain(context:Context,train:Train){
        val trainJson = Gson().toJson(train)
        var trainCount = getPreferencies(context, PREFERENCIES_TRAIN).getInt(TRAIN_COUNT, 0)
        trainCount = trainCount + 1
        getEditor(context, PREFERENCIES_TRAIN).putString(TRAIN + "_" + trainCount, trainJson).apply()
        getEditor(context, PREFERENCIES_TRAIN).putInt(TRAIN_COUNT, trainCount).apply()

    }


    fun getTrains(context:Context):List<Train>{
        val trainList = ArrayList<Train>()
        val gson = Gson()
        val trainCount = getPreferencies(context, PREFERENCIES_TRAIN).getInt(TRAIN_COUNT, 0)

        for (i in 0..trainCount) {
            val trainJson = getPreferencies(context, PREFERENCIES_TRAIN).getString(TRAIN+ "_" + i, Const.EMPTY)

            if (trainJson != Const.EMPTY) {
                val train = gson.fromJson<Train>(trainJson, Train::class.java)
                trainList.add(train)

            }
        }
        return trainList
    }

    fun putNewOffer(context:Context,offer: Offer){
        val offerJson = Gson().toJson(offer)
        var newOfferCount = getPreferencies(context, PREFERENCIES_NEW_OFFER).getInt(NEW_OFFER_COUNT, 0)
        newOfferCount = newOfferCount + 1
        getEditor(context, PREFERENCIES_NEW_OFFER).putString(NEW_OFFER + "_" + newOfferCount, offerJson).apply()
        getEditor(context, PREFERENCIES_NEW_OFFER).putInt(NEW_OFFER_COUNT, newOfferCount).apply()
    }

    fun getNewOffers(context:Context):List<Offer>{
        val offerList = ArrayList<Offer>()
        val gson = Gson()
        val newOfferCount = getPreferencies(context, PREFERENCIES_NEW_OFFER).getInt(NEW_OFFER_COUNT, 0)

        for (i in 0..newOfferCount) {
            val offerJson = getPreferencies(context, PREFERENCIES_NEW_OFFER).getString(NEW_OFFER + "_" + i, Const.EMPTY)

            if (offerJson != Const.EMPTY) {
                val offer = gson.fromJson<Offer>(offerJson, Offer::class.java)
                offerList.add(offer)

            }
        }
        return offerList
    }

    fun clearNewOffers(context: Context){
        getEditor(context, PREFERENCIES_NEW_OFFER).clear().apply()
    }

    fun putNewUser(context: Context, user: User) {
        val userJson = Gson().toJson(user)
        var newUserCount = getPreferencies(context, PREFERENCIES_NEW_USER).getInt(NEW_USER_COUNT, 0)
        newUserCount = newUserCount + 1
        getEditor(context, PREFERENCIES_NEW_USER).putString(NEW_USER + "_" + newUserCount, userJson).apply()
        getEditor(context, PREFERENCIES_NEW_USER).putInt(NEW_USER_COUNT, newUserCount).apply()
    }



    fun getNewUsers(context: Context): List<User> {
        val userList = ArrayList<User>()
        val gson = Gson()
        val newUserCount = getPreferencies(context, PREFERENCIES_NEW_USER).getInt(NEW_USER_COUNT, 0)

        for (i in 0..newUserCount) {
            val userJson = getPreferencies(context, PREFERENCIES_NEW_USER).getString(NEW_USER + "_" + i, Const.EMPTY)
            if (userJson != Const.EMPTY) {
                val user = gson.fromJson<User>(userJson, User::class.java)
                userList.add(user)
            }
        }
        return userList
    }

    fun clearNewUsers(context: Context){
        getEditor(context, PREFERENCIES_NEW_USER).clear().apply()
    }

    fun putUsers(context: Context, users: List<User>) {
        val gson = Gson()

        getEditor(context, PREFERENCIES_USER).clear().apply()
        getEditor(context, PREFERENCIES_USER).putInt(USER_COUNT, users.size).apply()

        for (i in 0..(users.size-1)) {
            val userJson = gson.toJson(users[i])
            getEditor(context, PREFERENCIES_USER).putString(USER + "_" + i.toString(), userJson).apply()
        }
    }

    fun getUsers(context: Context): List<User> {
        val userList = ArrayList<User>()
        val gson = Gson()

        val userCount = getPreferencies(context, PREFERENCIES_USER).getInt(USER_COUNT, 0)

        for (i in 0..userCount) {
            val userJson = getPreferencies(context, PREFERENCIES_USER).getString(USER + "_" + i.toString(), Const.EMPTY)
            if (userJson != Const.EMPTY) {
                val user = gson.fromJson<User>(userJson, User::class.java)
                userList.add(user)
            }
        }
        return userList
    }

    fun updateUser(context:Context,userUpdate:User):Boolean{
        val gson = Gson()

        val userCount = getPreferencies(context, PREFERENCIES_USER).getInt(USER_COUNT, 0)
        for (i in 0..userCount) {
            val userJson = getPreferencies(context, PREFERENCIES_USER).getString(USER + "_" + i.toString(), Const.EMPTY)
            if (userJson != Const.EMPTY) {
                val user = gson.fromJson<User>(userJson, User::class.java)
                if(user.id == userUpdate.id){
                    val userUpdateJson = gson.toJson(userUpdate)
                    getEditor(context, PREFERENCIES_USER).putString(USER + "_" + i.toString(), userUpdateJson).apply()
                    return true
                }
            }
        }
        return false
    }

}