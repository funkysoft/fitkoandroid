package eu.funkysoft.fitkomobile.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import eu.funkysoft.fitkomobile.api.Model.User
import eu.funkysoft.fitkomobile.R
import eu.funkysoft.fitkomobile.Tool

class UserAdapter(val userList: List<User>, val activity: Context, val listener: OnUserEvent) : RecyclerView.Adapter<UserAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val rootView = LayoutInflater.from(activity).inflate(R.layout.item_user, parent, false)
        val textName = rootView.findViewById<TextView>(R.id.text_user_name)
        val textEmail = rootView.findViewById<TextView>(R.id.text_user_email)
        val textPhone = rootView.findViewById<TextView>(R.id.text_user_phone)
        val textEnterKind = rootView.findViewById<TextView>(R.id.text_enter_type)
        val textEnterValue = rootView.findViewById<TextView>(R.id.text_enter_value)
        val imageNew = rootView.findViewById<ImageView>(R.id.image_new)

        return ViewHolder(rootView, textName, textEmail, textPhone, textEnterKind, textEnterValue, imageNew)
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val user = userList[position]

        holder!!.textName.text = user.name
        holder.textEmail.text = user.email
        holder.textPhone.text = user.phone
        holder.textEnterKind.text = Tool.getEnterType(activity, user.acces_type)
        holder.textEnterValue.text = Tool.getEnterValue(user)

        if(user.isNew)
            holder.imageNew.visibility = View.VISIBLE
        else
            holder.imageNew.visibility = View.INVISIBLE

        holder.rootView.setOnClickListener {
            listener.onUserClick(user)
        }
    }

    class ViewHolder(val rootView: View,
                     val textName: TextView,
                     val textEmail: TextView,
                     val textPhone: TextView,
                     val textEnterKind: TextView,
                     val textEnterValue: TextView,
                     val imageNew: ImageView) : RecyclerView.ViewHolder(rootView)

    interface OnUserEvent {
        fun onUserClick(user: User)
    }
}