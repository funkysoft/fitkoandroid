package eu.funkysoft.fitkomobile.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import eu.funkysoft.fitkomobile.activity.EditEntersActivity
import eu.funkysoft.fitkomobile.api.Model.User
import eu.funkysoft.fitkomobile.R

class TrainingsAdapter(val userList: List<User>, val activity: EditEntersActivity) : RecyclerView.Adapter<TrainingsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val rootView = LayoutInflater.from(activity).inflate(R.layout.item_user, parent, false)
        val textName = rootView.findViewById<TextView>(R.id.text_user_name)
        val textDate = rootView.findViewById<TextView>(R.id.text_train_date)
        val textTime= rootView.findViewById<TextView>(R.id.text_train_from_to)


        return ViewHolder(rootView,textName,textDate,textTime)
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val user = userList[position]

        holder!!.textName.text = user.name


        holder.rootView.setOnClickListener {

        }
    }

    class ViewHolder(val rootView: View,
                     val textName: TextView,
                     val textDate: TextView,
                     val textMail: TextView) : RecyclerView.ViewHolder(rootView)

}