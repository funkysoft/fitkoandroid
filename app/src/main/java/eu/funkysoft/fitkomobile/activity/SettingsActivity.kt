package eu.funkysoft.fitkomobile.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import eu.funkysoft.fitkomobile.api.ApiClient
import eu.funkysoft.fitkomobile.api.Model.User
import eu.funkysoft.fitkomobile.DataPreferencies
import eu.funkysoft.fitkomobile.dialogs.ProgressDialog
import eu.funkysoft.fitkomobile.R
import eu.funkysoft.fitkomobile.api.Model.BaseResponse
import eu.funkysoft.fitkomobile.api.Model.Offer
import eu.funkysoft.fitkomobile.api.Model.SaveDataRequest
import eu.funkysoft.fitkomobile.dialogs.IpDialog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class SettingsActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var buttonLoadData: Button
    lateinit var buttonSaveData: Button
    lateinit var buttonSetIp: Button
    lateinit var progressDialog: ProgressDialog
    lateinit var layoutNewData: View

    lateinit var users: List<User>
    lateinit var offers: List<Offer>
    lateinit var textIp: TextView


    val fitkoApi by lazy {
        ApiClient.create(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        buttonLoadData = findViewById(R.id.button_load_data)
        buttonLoadData.setOnClickListener(this)

        buttonSaveData = findViewById(R.id.button_put_data)
        buttonSaveData.setOnClickListener(this)

        buttonSetIp = findViewById(R.id.button_set_ip)
        buttonSetIp.setOnClickListener(this)

        layoutNewData = findViewById(R.id.layout_new_data)

        progressDialog = ProgressDialog(this)

        users = DataPreferencies.getNewUsers(this)
        offers = DataPreferencies.getNewOffers(this)

        textIp = findViewById<TextView>(R.id.text_ip_address)
        textIp.text = "IP: " + DataPreferencies.getIp(this)

        showNewDataInfo()
    }

    fun showNewDataInfo() {

        if (!users.isEmpty() || !offers.isEmpty()) {
            layoutNewData.visibility = View.VISIBLE
        } else {
            layoutNewData.visibility = View.GONE
        }

    }

    fun showError(error: Throwable) {
        progressDialog.finishFail(error.toString())
        when (error) {
            is SocketTimeoutException -> {
                Log.i("TAGEX", "")
            }
            is UnknownHostException -> {
                Log.i("TAGEX", "")
            }
            is HttpException -> {
                Log.i("TAGEX", "")
            }
        }
    }

    fun putUsersToStorage(users: List<User>) {
        DataPreferencies.putUsers(this, users)
        progressDialog.finishOk(R.string.load_data_success)
    }

    fun getUsers() {
        progressDialog.startProgress()
        fitkoApi.getUsers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result -> putUsersToStorage(result) },
                        { error -> showError(error) }

                )
    }

    fun saveNewUsers() {
        progressDialog.startProgress()
        var request = SaveDataRequest()
        request.users = DataPreferencies.getNewUsers(this)
        request.offers = DataPreferencies.getNewOffers(this)
        fitkoApi.saveUsers(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result -> saveUserFinish(result) },
                        { error -> showError(error) }

                )
    }

    fun saveUserFinish(response: BaseResponse) {
        DataPreferencies.clearNewUsers(this)
        DataPreferencies.clearNewOffers(this)
        users = DataPreferencies.getNewUsers(this)
        offers = DataPreferencies.getNewOffers(this)
        showNewDataInfo()
        progressDialog.finishOk(R.string.store_data_success)
    }

    override fun onClick(v: View?) {
        if (v == buttonLoadData) {
            getUsers()
        }

        if (v == buttonSaveData) {
            saveNewUsers()
        }

        if (v == buttonSetIp) {
            IpDialog(this,textIp).show()
        }
    }
}
