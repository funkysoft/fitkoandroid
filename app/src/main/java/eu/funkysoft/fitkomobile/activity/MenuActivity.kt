package eu.funkysoft.fitkomobile.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import eu.funkysoft.fitkomobile.DataPreferencies
import eu.funkysoft.fitkomobile.dialogs.IpDialog
import eu.funkysoft.fitkomobile.R

class MenuActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var buttonEditEnters: Button
    lateinit var buttonNewUser: Button
    lateinit var buttonTrainings:Button
    lateinit var buttonSettings: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        buttonEditEnters = findViewById(R.id.button_edit_enters)
        buttonEditEnters.setOnClickListener(this)

        buttonNewUser = findViewById(R.id.button_new_user)
        buttonNewUser.setOnClickListener(this)

        buttonTrainings = findViewById(R.id.button_trainings)
        buttonTrainings.setOnClickListener(this)

        buttonSettings = findViewById(R.id.button_settings)
        buttonSettings.setOnClickListener(this)

        if(DataPreferencies.getIp(this) == Const.EMPTY)
            IpDialog(this).show()
    }

    override fun onClick(v: View?) {
        if (v == buttonEditEnters) {
            startActivity(Intent(this, EditEntersActivity::class.java))
        }

        if (v == buttonNewUser) {
            startActivity(Intent(this, NewUserActivity::class.java))
        }

        if(v == buttonTrainings){
            startActivity(Intent(this, TrainActivity::class.java))
        }

        if(v == buttonSettings){
            startActivity(Intent(this,SettingsActivity::class.java))
        }
    }
}
