package eu.funkysoft.fitkomobile.activity

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.app.PendingIntent
import android.nfc.NfcAdapter
import android.content.IntentFilter
import android.view.View
import eu.funkysoft.fitkomobile.R
import eu.funkysoft.fitkomobile.Tool
import android.app.DatePickerDialog
import android.widget.*
import eu.funkysoft.fitkomobile.api.Model.User
import eu.funkysoft.fitkomobile.DataPreferencies
import eu.funkysoft.fitkomobile.dialogs.InfoDialog
import java.util.*


class NewUserActivity : AppCompatActivity(), View.OnClickListener, DatePickerDialog.OnDateSetListener, AdapterView.OnItemSelectedListener {
    var nfcAdapter: NfcAdapter? = null
    var nfcPendingIntent: PendingIntent? = null

    lateinit var editTextName: EditText
    lateinit var editTextEmail: EditText
    lateinit var editTextPhone: EditText
    lateinit var editTextAddress: EditText
    lateinit var editTextCount: EditText

    lateinit var textCardCode: TextView
    lateinit var textDate: TextView

    lateinit var layoutDate: View
    lateinit var layoutCount: View

    lateinit var spinnerEnterKind: Spinner

    lateinit var buttonAddUser: Button

    var enterKind = 0
    var date = Const.EMPTY

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_user)

        editTextName = findViewById(R.id.editext_name)
        editTextEmail = findViewById(R.id.editext_email)
        editTextPhone = findViewById(R.id.editext_phone)
        editTextAddress = findViewById(R.id.editext_address)
        editTextCount = findViewById(R.id.editext_enter_count)

        textCardCode = findViewById(R.id.textview_card_code)
        textDate = findViewById(R.id.textview_enter_date)
        textDate.setOnClickListener(this)

        layoutCount = findViewById(R.id.layout_enter_count)
        layoutDate = findViewById(R.id.layout_enter_date)

        spinnerEnterKind = findViewById(R.id.spinner_enters)

        buttonAddUser = findViewById(R.id.button_add_user)
        buttonAddUser.setOnClickListener(this)

        nfcAdapter = NfcAdapter.getDefaultAdapter(this)
        nfcPendingIntent = PendingIntent.getActivity(this, 0, Intent(this, this.javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0)

        spinnerEnterKind.onItemSelectedListener = this
    }

    fun hideAll() {
        layoutDate.visibility = View.GONE
        layoutCount.visibility = View.GONE
    }

    fun showDate() {
        layoutDate.visibility = View.VISIBLE
        layoutCount.visibility = View.GONE
    }

    fun showCount() {
        layoutDate.visibility = View.GONE
        layoutCount.visibility = View.VISIBLE
    }

    fun addUser() {
        if (checkInputData()) {
            val user = User()
            user.name = editTextName.text.toString()
            user.email = editTextEmail.text.toString()
            user.phone = editTextPhone.text.toString()
            user.address = editTextAddress.text.toString()
            user.card_code = textCardCode.text.toString()
            user.isNew = true

            user.acces_type = enterKind
            if (enterKind == Const.ENTER_COUNT)
                user.acces_count = Integer.parseInt(editTextCount.text.toString())
            if (enterKind == Const.ENTYER_DATE)
                user.allow_time = date
            DataPreferencies.putNewUser(this, user)
            clearInputData()
            InfoDialog(this, getString(R.string.user_was_succesfull_added),1).show()
        } else {
            InfoDialog(this, getString(R.string.please_fill_all_info),2).show()
        }
    }

    fun clearInputData(){
        textCardCode.text = Const.EMPTY
        editTextName.setText(Const.EMPTY)
        editTextEmail.setText(Const.EMPTY)
        editTextPhone.setText(Const.EMPTY)
        editTextAddress.setText(Const.EMPTY)
        enterKind = 0
        spinnerEnterKind.setSelection(0)
    }

    fun checkInputData(): Boolean {
        if (textCardCode.text == Const.EMPTY) return false
        if (editTextName.text.toString() == Const.EMPTY) return false
        if (editTextEmail.text.toString() == Const.EMPTY) return false
        if (editTextPhone.text.toString() == Const.EMPTY) return false
        if (editTextAddress.text.toString() == Const.EMPTY) return false

        if (enterKind == 0)
            return false
        else if (enterKind == Const.ENTER_COUNT) {
            if (editTextCount.text.toString() == Const.EMPTY)
                return false
        } else if (enterKind == Const.ENTYER_DATE) {
            if (textDate.text.toString() == Const.EMPTY)
                return false
        }
        return true
    }

    @SuppressLint("SetTextI18n")
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        textDate.text = dayOfMonth.toString() +
                "." + (month + 1).toString() +
                "." + year.toString()

        date = year.toString() + "-" + (month + 1).toString() + "-" + dayOfMonth.toString() + " " + "23:59:59"
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        enterKind = 0
        hideAll()
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        enterKind = position
        when (position) {
            0 -> hideAll()
            1 -> showCount()
            2 -> showDate()
        }
    }

    override fun onClick(v: View?) {
        if (v == textDate) {
            var myCalendar = Calendar.getInstance()
            DatePickerDialog(this@NewUserActivity, this@NewUserActivity, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show()
        }

        if (v == buttonAddUser) {
            addUser()
        }
    }

    override fun onResume() {
        super.onResume()
        val tagDetected = IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED) // filter for tags
        val writeTagFilters = arrayOf(tagDetected)
        nfcAdapter?.enableForegroundDispatch(this, nfcPendingIntent, writeTagFilters, null)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)

        if (NfcAdapter.ACTION_TAG_DISCOVERED == intent.action) {
            textCardCode.text = Tool.getCardCode(intent)
            Tool.vibrate(this)
        }
    }
}
