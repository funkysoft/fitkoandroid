package eu.funkysoft.fitkomobile.activity

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.app.PendingIntent
import android.nfc.NfcAdapter
import android.content.IntentFilter
import android.support.annotation.IntegerRes
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import eu.funkysoft.fitkomobile.adapters.UserAdapter
import eu.funkysoft.fitkomobile.adapters.UserAdapter.OnUserEvent
import eu.funkysoft.fitkomobile.api.Model.User
import eu.funkysoft.fitkomobile.DataPreferencies
import eu.funkysoft.fitkomobile.dialogs.InfoDialog
import eu.funkysoft.fitkomobile.R
import eu.funkysoft.fitkomobile.Tool
import eu.funkysoft.fitkomobile.api.Model.Offer
import eu.funkysoft.fitkomobile.dialogs.ProgressDialog
import java.util.*

class EditEntersActivity : AppCompatActivity(),
        View.OnClickListener,
        AdapterView.OnItemSelectedListener,
        DatePickerDialog.OnDateSetListener,
        TextWatcher {
    var nfcAdapter: NfcAdapter? = null
    var nfcPendingIntent: PendingIntent? = null
    lateinit var linearLayoutManager: GridLayoutManager

    lateinit var editTextSearch: EditText
    lateinit var textDate: TextView
    lateinit var textUserDescription: TextView
    lateinit var editTextCount: EditText

    lateinit var recycleViewUsers: RecyclerView
    lateinit var spinnerEnterKind: Spinner

    lateinit var layoutDate: View
    lateinit var layoutCount: View

    lateinit var buttonEditEntry: Button

    var userList = ArrayList<User>()
    var enterKind = 0
    var userSelected: User? = null
    var date = Const.EMPTY

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_enters)

        editTextSearch = findViewById(R.id.editext_search)
        editTextSearch.addTextChangedListener(this)
        recycleViewUsers = findViewById(R.id.recycleview_users)

        buttonEditEntry = findViewById(R.id.button_edit_entry)
        buttonEditEntry.setOnClickListener(this)

        editTextCount = findViewById(R.id.editext_enter_count)

        textDate = findViewById(R.id.textview_enter_date)
        textDate.setOnClickListener(this)

        textUserDescription = findViewById(R.id.text_user_description)

        layoutCount = findViewById(R.id.layout_enter_count)
        layoutDate = findViewById(R.id.layout_enter_date)

        spinnerEnterKind = findViewById(R.id.spinner_enters)
        spinnerEnterKind.onItemSelectedListener = this

        linearLayoutManager = GridLayoutManager(this, 1)
        recycleViewUsers.layoutManager = linearLayoutManager

        userList = DataPreferencies.getNewUsers(this) as ArrayList<User>
        userList.addAll(DataPreferencies.getUsers(this) as ArrayList<User>)
        addDataToList(userList)

        nfcAdapter = NfcAdapter.getDefaultAdapter(this)
        nfcPendingIntent = PendingIntent.getActivity(this, 0, Intent(this, this.javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0)
    }

    fun hideAll() {
        layoutDate.visibility = View.GONE
        layoutCount.visibility = View.GONE
    }

    fun showDate() {
        layoutDate.visibility = View.VISIBLE
        layoutCount.visibility = View.GONE
    }

    fun showCount() {
        layoutDate.visibility = View.GONE
        layoutCount.visibility = View.VISIBLE
    }

    fun addDataToList(users: List<User>) {
        recycleViewUsers.adapter = UserAdapter(users, this, object : OnUserEvent {
            override fun onUserClick(user: User) {
                setUserDescription(user)
            }
        })
    }

    fun setUserDescription(user: User) {
        userSelected = user
        textUserDescription.text = user.name + " -> " + Tool.getEnterValue(user)
        spinnerEnterKind.setSelection(user.acces_type)
        if (user.acces_type == Const.ENTER_COUNT) editTextCount.setText(user.acces_count.toString())
        if (user.acces_type == Const.ENTYER_DATE) textDate.text = user.allow_time
    }

    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        if (s.toString() != Const.EMPTY) {
            val tempList = ArrayList<User>()
            userList.forEach {
                if (it.name!!.toLowerCase().contains(s.toString().toLowerCase())) {
                    tempList.add(it)
                }
            }
            addDataToList(tempList)
        } else {
            addDataToList(userList)
        }
    }

    override fun onResume() {
        super.onResume()
        val tagDetected = IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED) // filter for tags
        val writeTagFilters = arrayOf(tagDetected)
        nfcAdapter?.enableForegroundDispatch(this, nfcPendingIntent, writeTagFilters, null)
    }

    override fun onClick(v: View?) {
        if (v == textDate) {
            var myCalendar = Calendar.getInstance()
            DatePickerDialog(this@EditEntersActivity, R.style.myDatePickerStyle, this@EditEntersActivity, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show()
        }

        if (v == buttonEditEntry) {
            if (userSelected != null) {

                val offer = Offer()
                offer.isNew = true
                offer.user = userSelected!!.id
                offer.acces_type = enterKind

                userSelected!!.acces_type = enterKind
                if(enterKind == Const.ENTER_COUNT){
                    userSelected!!.acces_count = Integer.parseInt(editTextCount.text.toString())
                    offer.acces_count = Integer.parseInt(editTextCount.text.toString())
                }else{
                    userSelected!!.allow_time = date
                    offer.allow_time = date
                }

                DataPreferencies.updateUser(this, userSelected!!)
                DataPreferencies.putNewOffer(this,offer)

                userList = DataPreferencies.getNewUsers(this) as ArrayList<User>
                userList.addAll(DataPreferencies.getUsers(this) as ArrayList<User>)
                addDataToList(userList)
                clearInputData()
                var dialog = InfoDialog(this,getString(R.string.change_has_been_set),1)
                dialog.show()
            }else{
                InfoDialog(this, getString(R.string.please_fill_all_info),2).show()
            }
        }

    }

    fun clearInputData(){
        editTextSearch.setText(Const.EMPTY)
        textDate.text = Const.EMPTY
        textUserDescription.text = Const.EMPTY
        editTextCount.setText(Const.EMPTY)

        spinnerEnterKind.setSelection(0)
        enterKind = 0
        userSelected = null
        date = Const.EMPTY
    }

    @SuppressLint("SetTextI18n")
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        date = year.toString() + "-" + (month + 1).toString() + "-" + dayOfMonth.toString()+" 23:59:59"
        textDate.text = dayOfMonth.toString() +
                "." + (month + 1).toString() +
                "." + year.toString()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        enterKind = 0
        hideAll()
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        enterKind = position
        when (position) {
            0 -> hideAll()
            1 -> showCount()
            2 -> showDate()
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        if (NfcAdapter.ACTION_TAG_DISCOVERED == intent.action) {
            Tool.vibrate(this)
            val cardCode = Tool.getCardCode(intent)
            var userDetected: User? = null
            userList.forEach {
                if (it.card_code == cardCode) {
                    userDetected = it
                }
            }

            if (userDetected == null) {
                InfoDialog(this, getString(R.string.user_with_card_not_found) + " " + cardCode, 0).show()
            } else {
                setUserDescription(userDetected!!)
            }

        }
    }
}
