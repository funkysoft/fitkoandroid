package eu.funkysoft.fitkomobile.activity

import android.app.DatePickerDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import eu.funkysoft.fitkomobile.api.Model.Train
import eu.funkysoft.fitkomobile.api.Model.User
import eu.funkysoft.fitkomobile.DataPreferencies
import eu.funkysoft.fitkomobile.dialogs.InfoDialog
import eu.funkysoft.fitkomobile.dialogs.TimeDialog
import eu.funkysoft.fitkomobile.dialogs.UsersDialog
import eu.funkysoft.fitkomobile.R
import eu.funkysoft.fitkomobile.dialogs.ProgressDialog
import java.util.*

class TrainActivity : AppCompatActivity(), View.OnClickListener, DatePickerDialog.OnDateSetListener {

    lateinit var textUser: TextView
    lateinit var textDate: TextView
    lateinit var textTime: TextView
    lateinit var editTextTime: EditText
    lateinit var buttonAddTrain: Button

    var selectedUser: User? = null
    var date: String = Const.EMPTY
    var time: String = Const.EMPTY

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_train)

        textUser = findViewById(R.id.text_train_user)
        textUser.setOnClickListener(this)

        textDate = findViewById(R.id.text_train_date)
        textDate.setOnClickListener(this)

        textTime = findViewById(R.id.text_train_time)
        textTime.setOnClickListener(this)

        editTextTime = findViewById(R.id.text_train_duration)

        buttonAddTrain = findViewById(R.id.button_add_training)
        buttonAddTrain.setOnClickListener(this)

    }

    fun clearInputData() {
        date = Const.EMPTY
        textTime.text = Const.EMPTY
        selectedUser = null
    }

    fun checkInputData(): Boolean {
        if (selectedUser == null) return false
        if (date == Const.EMPTY) return false
        if (textTime.text == Const.EMPTY) return false
        if (editTextTime.text.toString() == Const.EMPTY) return false
        return true

    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        date = year.toString() + "-" + (month + 1).toString() + "-" + dayOfMonth.toString()
        textDate.text = dayOfMonth.toString() +
                "." + (month + 1).toString() +
                "." + year.toString()
    }

    override fun onClick(v: View?) {
        if (v == textUser) {
            UsersDialog(this, object : UsersDialog.OnUserChoose {
                override fun onUserClick(user: User) {
                    selectedUser = user
                    textUser.text = user.name
                }

            }).show()
        }
        if (v == textDate) {
            var myCalendar = Calendar.getInstance()
            DatePickerDialog(this@TrainActivity,R.style.myDatePickerStyle, this@TrainActivity, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show()
        }



        if(v == textTime){
            TimeDialog(this,object : TimeDialog.OnTimechoose{
                override fun onTimeChoose(timeValue: String) {
                    time = "$timeValue:00"
                    textTime.text = timeValue
                }
            }).show()
        }

        if (v == buttonAddTrain) {
            if (checkInputData()) {
                val train = Train()
                train.duration = Integer.parseInt(editTextTime.text.toString())
                train.date = date
                train.time = time
                train.user = selectedUser

                DataPreferencies.putTrain(this,train)
                InfoDialog(this, getString(R.string.train_was_succesfull_added),1).show()
                clearInputData()
            } else {
                InfoDialog(this, getString(R.string.please_choose_user),2).show()
            }
        }
    }

}
