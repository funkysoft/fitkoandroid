package eu.funkysoft.fitkomobile

import android.content.Context
import android.content.Intent
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import eu.funkysoft.fitkomobile.activity.Const
import eu.funkysoft.fitkomobile.api.Model.User

/**
 * Created by Funky on 1/18/2019.
 */
object Tool {

    fun getEnterType(context: Context, enterType: Int): String {
        if (enterType == Const.ENTER_COUNT)
            return context.getString(R.string.enter_count)
        return context.getString(R.string.enter_date)
    }

    fun getEnterValue(user: User): String? {
        if (user.acces_type == Const.ENTER_COUNT)
            return user.acces_count.toString()
        return user.allow_time
    }

    fun getCardCode(intent: Intent): String {
        val tagFromIntent = intent.getParcelableExtra<Tag>(NfcAdapter.EXTRA_TAG)
        val extraID = tagFromIntent.getId()
        val sb = StringBuilder()
        for (b in extraID) {
            sb.append(String.format("%02X", b))
        }
        return sb.toString()
    }

    fun vibrate(context: Context) {
        val v = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            v.vibrate(500)
        }
    }

}