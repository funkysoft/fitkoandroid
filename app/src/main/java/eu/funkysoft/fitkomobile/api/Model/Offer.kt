package eu.funkysoft.fitkomobile.api.Model

import eu.funkysoft.fitkomobile.activity.Const

/**
 * Created by Funky on 1/21/2019.
 */
class Offer {
    var user:Int = 0
    var acces_type:Int = 0
    var acces_count:Int = 0
    var allow_time:String = Const.EMPTY
    var isNew:Boolean = false
}