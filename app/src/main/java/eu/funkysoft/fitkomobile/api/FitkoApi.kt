package eu.funkysoft.fitkomobile.api

import eu.funkysoft.fitkomobile.api.Model.BaseResponse
import eu.funkysoft.fitkomobile.api.Model.SaveDataRequest
import eu.funkysoft.fitkomobile.api.Model.User
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST


/**
 * Created by Funky on 1/18/2019.
 */
interface FitkoApi {
    @GET("getusers")
    fun getUsers(): Observable<ArrayList<User>>

    @POST("addusersmobile")
    fun saveUsers(@Body request:SaveDataRequest): Observable<BaseResponse>
}