package eu.funkysoft.fitkomobile.api.Model

import eu.funkysoft.fitkomobile.activity.Const

/**
 * Created by Funky on 1/18/2019.
 */
class User {

    var acces_count: Int = 0
    var allow_time: String? = Const.EMPTY
    var acces_type: Int = 0
    var card_code: String? = Const.EMPTY
    var id: Int = 0
    var name: String? = Const.EMPTY
    var phone: String? = Const.EMPTY
    var address: String? = Const.EMPTY
    var email: String? = Const.EMPTY
    var reg_time: String? = Const.EMPTY
    var isNew:Boolean = false
}