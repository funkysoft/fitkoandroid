package eu.funkysoft.fitkomobile.api

import android.content.Context
import eu.funkysoft.fitkomobile.activity.Const
import eu.funkysoft.fitkomobile.DataPreferencies
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Funky on 1/19/2019.
 */
object ApiClient {
    val END_POINT = "/FitkoServer/www/api/javaapp/"
    val DEFAULT_IP = "10.0.0.139"

    fun create(context:Context):FitkoApi{
        var ip = DataPreferencies.getIp(context)
        if(ip == Const.EMPTY)
            ip = DEFAULT_IP

        val url = "http://"+ip+ END_POINT

        val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(url)
                .build()

        return retrofit.create(FitkoApi::class.java)
    }
}