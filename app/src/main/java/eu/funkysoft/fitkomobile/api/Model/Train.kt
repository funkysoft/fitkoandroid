package eu.funkysoft.fitkomobile.api.Model

import eu.funkysoft.fitkomobile.activity.Const

class Train {
    var user:User? = null
    var date:String = Const.EMPTY
    var time:String = Const.EMPTY

    // duration of train in minits
    var duration = 0
}