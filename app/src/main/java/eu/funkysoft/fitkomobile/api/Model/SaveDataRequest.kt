package eu.funkysoft.fitkomobile.api.Model

/**
 * Created by Funky on 1/27/2019.
 */
class SaveDataRequest {
    var users:List<User> = ArrayList()
    var offers:List<Offer> = ArrayList()
}