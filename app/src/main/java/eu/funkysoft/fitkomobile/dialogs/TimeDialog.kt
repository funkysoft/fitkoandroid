package eu.funkysoft.fitkomobile.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TimePicker
import eu.funkysoft.fitkomobile.R

class TimeDialog(context: Context?, var listener: OnTimechoose) : Dialog(context), View.OnClickListener {

    lateinit var timePicker: TimePicker
    lateinit var buttonOk: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_time)
        timePicker = findViewById(R.id.timepicker)
        timePicker.setIs24HourView(true)

        buttonOk = findViewById(R.id.button_time_dialog)
        buttonOk.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v == buttonOk) {
            val time = timePicker.hour.toString() + ":" + timePicker.minute.toString()
            listener.onTimeChoose(time)
            dismiss()
        }
    }

    interface OnTimechoose {
        fun onTimeChoose(time: String)
    }
}