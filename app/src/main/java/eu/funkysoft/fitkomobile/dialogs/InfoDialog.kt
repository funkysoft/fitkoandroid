package eu.funkysoft.fitkomobile.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import eu.funkysoft.fitkomobile.R

class InfoDialog(context: Context?, val infotext:String,val state:Int) : Dialog(context) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_info)

        val textInfo = findViewById<TextView>(R.id.text_info_dialog)
        textInfo.text = infotext

        val imageStatus = findViewById<ImageView>(R.id.image_status)

        if(state ==0){
            imageStatus.visibility = View.GONE
        }

        if(state ==1){
            imageStatus.visibility = View.VISIBLE
            imageStatus.setImageResource(R.drawable.icon_ok)
        }

        if(state ==2){
            imageStatus.visibility = View.VISIBLE
            imageStatus.setImageResource(R.drawable.icon_fail)
        }
    }
}