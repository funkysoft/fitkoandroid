package eu.funkysoft.fitkomobile.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import eu.funkysoft.fitkomobile.DataPreferencies
import eu.funkysoft.fitkomobile.R

class IpDialog(context: Context?) : Dialog(context), View.OnClickListener {

    constructor(context: Context?, text: TextView) : this(context) {
        TYPE = 1
        this.text = text
    }

    lateinit var buttonSave: Button
    lateinit var buttonCancal: Button
    lateinit var editextIp: EditText
    var text: TextView = TextView(context)
    var TYPE = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_ip)
        setCancelable(false)

        buttonSave = findViewById(R.id.button_save_ip)
        buttonSave.setOnClickListener(this)

        buttonCancal = findViewById(R.id.button_cancal_ip)
        buttonCancal.setOnClickListener(this)

        editextIp = findViewById(R.id.editext_ip)

    }

    override fun onClick(v: View?) {
        if (v == buttonCancal) {
            dismiss()
        }

        if (v == buttonSave) {
            if(TYPE == 1)text.text = "IP: " + editextIp.text.toString()
            DataPreferencies.saveIp(context, editextIp.text.toString())
            dismiss()
        }
    }
}