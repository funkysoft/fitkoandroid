package eu.funkysoft.fitkomobile.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import eu.funkysoft.fitkomobile.adapters.UserAdapter
import eu.funkysoft.fitkomobile.api.Model.User
import eu.funkysoft.fitkomobile.DataPreferencies
import eu.funkysoft.fitkomobile.R
import java.util.ArrayList

class UsersDialog(context: Context?, var listener:OnUserChoose) : Dialog(context) {

    lateinit var linearLayoutManager: GridLayoutManager
    lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_users)

        recyclerView = findViewById(R.id.recycleview_diealog_users)
        linearLayoutManager = GridLayoutManager(context, 1)
        recyclerView.layoutManager = linearLayoutManager

        var userList = DataPreferencies.getNewUsers(context) as ArrayList<User>
        userList.addAll(DataPreferencies.getUsers(context) as ArrayList<User>)
        addDataToList(userList)
    }

    fun addDataToList(users: List<User>) {
        recyclerView.adapter = UserAdapter(users, context, object : UserAdapter.OnUserEvent {
            override fun onUserClick(user: User) {
                listener.onUserClick(user)
            }
        })
    }

    interface OnUserChoose{
        fun onUserClick(user:User)
    }
}